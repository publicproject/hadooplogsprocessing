spark-submit \
  --master yarn \
  --class org.tri.JavaFlumeEventCount \
  --jars lib/spark-examples-1.3.0-cdh5.4.2-hadoop2.6.0-cdh5.4.2.jar \
 "target/scala-2.10/stream-spark_2.10-0.1-SNAPSHOT.jar" \
 "$@"
