drop table log_json;
drop table logs_csv;
drop table logs_agr1;


--  STORED AS TEXTFILE
 


CREATE EXTERNAL TABLE logs_csv
(user_id int, time STRING, action STRING, destination STRING, auto STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ';' 
LOCATION '/flume/events.csv';




CREATE EXTERNAL TABLE logs_agr1
( time STRING, coun int, destination STRING, auto STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ';' 
LOCATION '/flume/events2.csv';



#CREATE EXTERNAL TABLE log_json
#(user_id int, time STRING, action STRING, destination STRING, auto STRING)
#  ROW FORMAT SERDE
#  'org.apache.hive.hcatalog.data.JsonSerDe'
#  LOCATION '/flume/events'
#  ;



