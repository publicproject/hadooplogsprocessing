#! /usr/bin/env python

from datetime import datetime
import json
import random
import os
import sys
import time
import urllib

class LogGenerator:
    DESTINATION={
	"Country":33,
	"Region":33,
	"City":33
    }
#    def __init__(self, ipgen):

    def write_qps(self, dest, qps):
	sleep = 1.0 / qps
	while True:
            self.write(dest, 1)
            time.sleep(sleep)

    def write(self, dest, count):
        for i in range(count):

            stime = datetime.now().strftime("%Y%m%d:%H:")
            itime = int(time.time())
            dest_id = random.randrange(10);
            coun = random.randrange(10)+1;
            destination = self.pick_weighted_key(self.DESTINATION)+str(dest_id)
            auto_id = random.randrange(256);
            auto="auto"+str(auto_id)

            dest.write("{\"time\":\"%(time)s\",\"coun\":\"%(coun)s\",\"destination\":\"%(destination)s\",\"auto\":\"%(auto)s\"}\n" %
                {'time':itime,'coun':coun,'destination':destination,'auto':auto})

            dest.flush()

    def pick_weighted_key(self, hash):
        total = 0
        for t in hash.values():
            total = total + t
        rand = random.randrange(total)

        running = 0
        for (key, weight) in hash.items():
            if rand >= running and rand < (running + weight):
                return key
            running = running + weight

        return hash.keys()[0]

LogGenerator().write_qps(sys.stdout, 10)

